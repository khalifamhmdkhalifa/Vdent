package com.example.khalifa.myapplication.view.interfaces;

public interface SelectedTimeObserver {
    void subscriber(SelectedTimeListener selectedTimeListener);

    void unSubscriber(SelectedTimeListener selectedTimeListener);

}
