package com.example.khalifa.myapplication.prsenters;

import android.os.AsyncTask;

import com.example.khalifa.myapplication.model.Constants;
import com.example.khalifa.myapplication.model.interactor.interfaces.MainScheduleInterActor;
import com.example.khalifa.myapplication.prsenters.interfaces.MainSchedulePresenter;
import com.example.khalifa.myapplication.view.interfaces.MainScheduleView;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainSchedulePresenterImpl extends MainSchedulePresenter<MainScheduleView,
        MainScheduleInterActor> {
    private static final int NUMBER_OF_EXTRA_DAYS_TO_BE_ADDED = 10;
    private static final String DATE_FORMAT = "dd MMM yyyy";
    private static final String DAY_NAME_FORMAT = "EEEE";
    private List<Date> dateList;
    private final long MILLI_SECONDS_IN_DAY = 86400000;
    private int selectedTime = Constants.TIME_AM;

    public MainSchedulePresenterImpl(MainScheduleView view) {
        super(view);
    }

    @Override
    public void onAmButtonClicked(boolean checked) {
        if (!checked || selectedTime == Constants.TIME_AM)
            return;
        selectedTime = Constants.TIME_AM;
        getView().showLoading();
        new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] objects) {
                ((SelectedTimeListener) getView().getActivity()).onSelectedTimeChanged(selectedTime);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                getView().updatePagerSelectedTime(selectedTime);
                getView().notifySchedulesPagerListChanged();
                getView().hideLoading();
            }
        }.execute();


    }

    @Override
    public void onPmButtonClicked(boolean checked) {
        if (!checked || selectedTime == Constants.TIME_PM)
            return;
        selectedTime = Constants.TIME_PM;
        getView().showLoading();

        new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] objects) {

                ((SelectedTimeListener) getView().getActivity())
                        .onSelectedTimeChanged(selectedTime);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                getView().updatePagerSelectedTime(selectedTime);
                getView().notifySchedulesPagerListChanged();
                getView().hideLoading();
            }
        }.execute();
    }

    @Override
    public void onDayViewed(int position) {
        setDateText(dateList.get(position));
        if (position == 0)
            handleAddingEarlierDates(dateList.get(position));
        if (position == dateList.size() - 1)
            handleAddingLaterDates(dateList.get(position));


    }

    private void setDateText(Date date) {
        //in onStart()
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
//date format is:  "Date-Month-Year Hour:Minutes am/pm"
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT); //Date and time
        String currentDate = sdf.format(calendar.getTime());
//Day of Name in full form like,"Saturday", or if you need the first three characters you have to put "EEE" in the date format and your result will be "Sat".
        SimpleDateFormat sdf_ = new SimpleDateFormat(DAY_NAME_FORMAT);
        String dayName = sdf_.format(date);
        getView().setDateText(dayName + " ," + currentDate);
    }

    private void handleAddingLaterDates(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        for (int i = 0; i < NUMBER_OF_EXTRA_DAYS_TO_BE_ADDED; i++) {
            calendar.setTimeInMillis((calendar.getTimeInMillis() + MILLI_SECONDS_IN_DAY));
            dateList.add(new Date(calendar.getTimeInMillis()));
        }
        getView().notifySchedulesPagerListChanged();
    }

    private void handleAddingEarlierDates(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        for (int i = 0; i < NUMBER_OF_EXTRA_DAYS_TO_BE_ADDED; i++) {
            calendar.setTimeInMillis((calendar.getTimeInMillis() - MILLI_SECONDS_IN_DAY));
            dateList.add(0, new Date(calendar.getTimeInMillis()));
        }
        getView().notifySchedulesPagerListChanged();
        getView().setCurrentSchedulePagerItem(10);
    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onViewCreated() {
        setupDateList();
        getView().setupSchedulePager(dateList, selectedTime);
    }

    private void setupDateList() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        dateList = new ArrayList<>();
        dateList.add(new Date(calendar.getTimeInMillis() - MILLI_SECONDS_IN_DAY));
        dateList.add(calendar.getTime());
        dateList.add(new Date(calendar.getTimeInMillis() + MILLI_SECONDS_IN_DAY));

    }

    @Override
    protected MainScheduleInterActor initInterActor() {
        return null;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }
}
