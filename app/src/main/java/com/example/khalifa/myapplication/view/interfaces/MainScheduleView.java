package com.example.khalifa.myapplication.view.interfaces;

import java.util.Date;
import java.util.List;

public interface MainScheduleView extends BaseFragmentView {
    void setupSchedulePager(List<Date> dateList, int selectedTime);

    void updatePagerSelectedTime(int selectedTime);

    void notifySchedulesPagerListChanged();

    void setCurrentSchedulePagerItem(int position);

    void setDateText(String value);
}
