package com.example.khalifa.myapplication.view.interfaces;

import android.util.Log;

import com.example.khalifa.myapplication.model.Response.BaseError;
import com.example.khalifa.myapplication.model.Response.BaseResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public abstract class BaseResponseViewSubscriber<T extends BaseResponse> extends BaseViewSubscriber<T> {

    public BaseResponseViewSubscriber(BaseView view, boolean showLoading, boolean showErrorMessage) {
        super(view, showLoading, showErrorMessage);
    }

    public Disposable subscribeObservable(Observable<T> observable) {
        return observable.subscribe(new Consumer<T>() {
            @Override
            public void accept(T result) {
                //on Result
                onResult(result);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                //on Error
                onError(throwable);
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                //onComplete
                onComplete();
            }
        }, new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) throws Exception {
                //onSubscribe
                onSubscribe(disposable);
            }
        });
    }


    protected void onError(ArrayList<BaseError> errors) {
        if (errors == null || errors.isEmpty())
            return;

        if(showLoading)
            view.hideLoading();
        if (showErrorMessage)
            view.showErrorMessage(errors.get(0).getErrorMSG());
        Log.e(getClass().getSimpleName(), errors.get(0).getErrorMSG());

    }

    @Override
    protected void onResult(T result) {
        if (result != null && result.isResult())
            onSuccess(result);
        else
            onError(result.getErrors());
    }
}