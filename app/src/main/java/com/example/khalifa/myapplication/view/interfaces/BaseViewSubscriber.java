package com.example.khalifa.myapplication.view.interfaces;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public abstract class BaseViewSubscriber<T extends Object> {
    protected BaseView view;
    protected boolean showLoading;
    protected boolean showErrorMessage;

    public Disposable subscribeObservable(Observable<T> observable) {
        return observable.subscribe(new Consumer<T>() {
            @Override
            public void accept(T result) {
                //on Result
                onResult(result);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                onError(throwable);
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                //onComplete
                onComplete();
            }
        }, new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) throws Exception {
                //onSubscribe
                onSubscribe(disposable);
            }
        });
    }

    public BaseViewSubscriber(BaseView view,
                              boolean showLoading, boolean showErrorMessage) {
        this.view = view;
        this.showLoading = showLoading;
        this.showErrorMessage = showErrorMessage;
    }

    protected void onError(Throwable throwable) {
        throwable.printStackTrace();
        if (showLoading)
            view.hideLoading();
        if (showErrorMessage)
            view.showErrorMessage(throwable.getMessage());
    }

    public abstract void onSuccess(T result);


    protected void onResult(T result) {
        onSuccess(result);
    }

    protected void onSubscribe(Disposable disposable) {
        if (showLoading)
            view.showLoading();
    }

    protected void onComplete() {
        if (showLoading)
            view.hideLoading();
    }
}