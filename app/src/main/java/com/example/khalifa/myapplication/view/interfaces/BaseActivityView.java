package com.example.khalifa.myapplication.view.interfaces;

public interface BaseActivityView extends BaseView {
    void finishActivity();
}
