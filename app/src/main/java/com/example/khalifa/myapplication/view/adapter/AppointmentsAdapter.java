package com.example.khalifa.myapplication.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.khalifa.myapplication.R;
import com.example.khalifa.myapplication.model.Entity.Appointment;
import com.example.khalifa.myapplication.model.Entity.BaseAppointment;
import com.example.khalifa.myapplication.model.Entity.EmptyAppointment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentsAdapter extends BaseRecyclerViewAdapter<BaseAppointment,
        AppointmentsAdapter.Holder> {
    private static final int TYPE_EMPTY = 1;

    public AppointmentsAdapter(Context context, List<BaseAppointment> items) {
        super(context, items);
    }

    @Override
    int getItemLayoutId(int viewType) {
        if (viewType == TYPE_EMPTY)
            return R.layout.list_item_empty_appointment;
        else
            return R.layout.list_item_appointment;
    }

    @Override
    Holder createHolder(View rootView, int viewType) {
        if (viewType == TYPE_EMPTY)
            return new Holder(rootView);
        return new NonEmptyHolder(rootView);
    }

    @Override
    void onBindViewHolder(Holder holder, BaseAppointment item, int position) {
        if (item instanceof EmptyAppointment)
            handleEmptyAppointment(holder, (EmptyAppointment) item);
        else
            handleNonEmptyAppointment((NonEmptyHolder) holder, (Appointment) item);
    }

    private void handleEmptyAppointment(Holder holder, EmptyAppointment item) {
    }

    private void handleNonEmptyAppointment(NonEmptyHolder holder, Appointment item) {
        holder.textViewPatientName.setText(
                item.getPatientFirstName() + " " + item.getPatientLastName());

        if (item.getPatientImage() != null)
            Glide.with(getContext()).load(item.getPatientImage()).into(
                    holder.imageViewPatientImage);
        else
            holder.imageViewPatientImage.setImageResource(R.drawable.ic_launcher_background);

        holder.checkBoxAppointment.setChecked(item.isCompleted());


    }

    @Override
    void onItemClicked(BaseAppointment item, int position) {
    }


    @Override
    public int getItemViewType(int position) {
        if (getItems().get(position) instanceof EmptyAppointment)
            return TYPE_EMPTY;
        return super.getItemViewType(position);
    }

    public class Holder extends RecyclerView.ViewHolder {

        public Holder(@NonNull View itemView) {
            super(itemView);
        }
    }


    public class NonEmptyHolder extends Holder {
        @BindView(R.id.textView_patient)
        TextView textViewPatientName;
        @BindView(R.id.imageView_patientImage)
        ImageView imageViewPatientImage;
        @BindView(R.id.checkBox_appointment)
        CheckBox checkBoxAppointment;

        public NonEmptyHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
