package com.example.khalifa.myapplication.prsenters;

import com.example.khalifa.myapplication.model.Constants;
import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.model.Response.BaseResponse;
import com.example.khalifa.myapplication.model.interactor.DayScheduleInterActorImpl;
import com.example.khalifa.myapplication.model.interactor.interfaces.DayScheduleInterActor;
import com.example.khalifa.myapplication.prsenters.interfaces.DaySchedulePresenter;
import com.example.khalifa.myapplication.view.interfaces.BaseResponseViewSubscriber;
import com.example.khalifa.myapplication.view.interfaces.DayScheduleView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.functions.Consumer;

public class DaySchedulePresenterImpl extends DaySchedulePresenter<
        DayScheduleView, DayScheduleInterActor> {
    private static final String DATE_FORMAT = "MM-dd-yyyy";
    private Date date;
    private int selectedTime;
    private ArrayList<DrDaySchedule> drsDaySchedules;


    @Override
    protected DayScheduleInterActor initInterActor() {
        return new DayScheduleInterActorImpl();
    }

    public DaySchedulePresenterImpl(DayScheduleView view) {
        super(view);
    }

    @Override
    public void setup(Date date, int activatedTime) {
        this.date = date;
        selectedTime = activatedTime;
        if (drsDaySchedules == null)
            loadScheduleData(date);
    }

    private void updateActivatedTime(ArrayList<DrDaySchedule> drDaySchedules) {
        int activatedAppointments = 0;

        if (selectedTime == Constants.TIME_AM)
            activatedAppointments = DrDaySchedule.AM_APPOINTMENTS;
        else
            activatedAppointments = DrDaySchedule.PM_APPOINTMENTS;

        for (DrDaySchedule schedule : drDaySchedules)
            schedule.setActiveAppointments(activatedAppointments);


    }


    private void loadScheduleData(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        addSubscribe(subscribeObservable(
                new BaseResponseViewSubscriber<BaseResponse<ArrayList<DrDaySchedule>>>(
                        getView(), true, true) {
                    @Override
                    public void onSuccess(BaseResponse<ArrayList<DrDaySchedule>> result) {
                        ArrayList<DrDaySchedule> resultList = result.getData();
                        if (resultList == null || resultList.size() == 0)
                            handleEmptyList();
                        else
                            handleAppointmentsList(resultList);
                    }
                }, getInterActor().getSchedule(getClinicId()
                        , getBranchId(), dateFormat.format(date), "",
                        "").doOnNext(new Consumer<BaseResponse<ArrayList<DrDaySchedule>>>() {
                    @Override
                    public void accept(BaseResponse<ArrayList<DrDaySchedule>> arrayListBaseResponse) throws Exception {
                        if (arrayListBaseResponse != null && arrayListBaseResponse.isResult()) {
                            for (DrDaySchedule drDaySchedule :
                                    arrayListBaseResponse.getData()) {
                                if (drDaySchedule.getAllAppointments() != null
                                        && !drDaySchedule.getAllAppointments().isEmpty()) {
                                    drDaySchedule.setupDate(drDaySchedule.getAllAppointments()
                                            .get(0));

                                    if (selectedTime == Constants.TIME_AM)
                                        drDaySchedule.setActiveAppointments(DrDaySchedule.AM_APPOINTMENTS);
                                    else
                                        drDaySchedule.setActiveAppointments(DrDaySchedule.PM_APPOINTMENTS);

                                }
                            }


                        }
                    }
                })));


    /*    addSubscribe(subscribeObservable(
                new BaseResponseViewSubscriber<BaseResponse<ArrayList<DrDaySchedule>>>(
                        getView(), true, true) {
                    @Override
                    public void onSuccess(BaseResponse<ArrayList<DrDaySchedule>> result) {
                        ArrayList<DrDaySchedule> resultList = result.getData();
                        if (resultList == null || resultList.size() == 0)
                            handleEmptyList();
                        else
                            handleAppointmentsList(resultList);
                    }
                }, getInterActor().getSchedule(date.getTime())));

  */
    }

    private void handleEmptyList() {
        drsDaySchedules = new ArrayList<>();
    }

    private void handleAppointmentsList(ArrayList<DrDaySchedule> resultList) {
        drsDaySchedules = resultList;
        if (!isViewAttached())
            return;
        getView().setupRecyclerView(drsDaySchedules);
    }
/*
    private ArrayList<DrDaySchedule> getDrsSchedulesList(ArrayList<Appointment> resultList) {
        HashMap<String, DrDaySchedule> drsMap = new HashMap<>();
        for (Appointment appointment : resultList) {
            String doctorUserID = appointment.getDoctorUserID();
            if (drsMap.containsKey(doctorUserID))
                addAppointment(appointment, drsMap.get(doctorUserID));
            else
                addNewDrSchedule(appointment, doctorUserID, drsMap);
        }
        return new ArrayList(drsMap.values());
    }

    private void addAppointment(Appointment appointment, DrDaySchedule schedule) {
        schedule.getAllAppointments().add(appointment);
    }
    private void addNewDrSchedule(Appointment appointment, String drName,
                                  HashMap<String, DrDaySchedule> drsMap) {
        ArrayList<Appointment> appointments = new ArrayList();
        appointments.add(appointment);
        DrDaySchedule drDaySchedule = new DrDaySchedule(appointments,
                new CustomDate(date));
        if (selectedTime == Constants.TIME_AM)
            drDaySchedule.setActiveAppointments(DrDaySchedule.AM_APPOINTMENTS);
        else
            drDaySchedule.setActiveAppointments(DrDaySchedule.PM_APPOINTMENTS);
        drsMap.put(drName, drDaySchedule);
    }
*/

    private String getBranchId() {
        return "Q1TKno3qmm6ecp1of72nSA==";
    }

    private String getClinicId() {
        return "r5Jy47oxh4dge6ia8LU27A==";
    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {
    }

    @Override
    public void onViewCreated() {

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {


    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onSelectedTimeChanged(int selectedTime) {
        if (this.selectedTime == selectedTime)
            return;
        this.selectedTime = selectedTime;
        if (drsDaySchedules == null)
            return;
        updateActivatedTime(drsDaySchedules);
        getView().notifySchedulesListChanged();
    }
}
