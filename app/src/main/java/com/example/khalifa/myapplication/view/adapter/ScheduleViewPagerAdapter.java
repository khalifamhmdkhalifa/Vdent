package com.example.khalifa.myapplication.view.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.example.khalifa.myapplication.view.fragments.DayScheduleFragment;

import java.util.Date;
import java.util.List;

public abstract class ScheduleViewPagerAdapter extends FragmentPagerAdapter {
    private List<Date> dateList;
    private int activatedTime;

    public ScheduleViewPagerAdapter(FragmentManager fm, List<Date> dates, int activatedTime) {
        super(fm);
        dateList = dates;
        this.activatedTime = activatedTime;
    }

    @Override
    public Fragment getItem(int position) {
        return DayScheduleFragment.newInstance(dateList.get(position), activatedTime);
    }

    @Override
    public int getCount() {
        return dateList != null ? dateList.size() : 0;
    }

    protected abstract void onItemViewed(int position, Date date);

    public void setActivatedTime(int activatedTime) {
        this.activatedTime = activatedTime;
    }



}
