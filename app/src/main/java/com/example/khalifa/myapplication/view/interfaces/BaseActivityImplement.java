package com.example.khalifa.myapplication.view.interfaces;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.khalifa.myapplication.prsenters.interfaces.BaseActivityPresenter;

import butterknife.ButterKnife;

public abstract class BaseActivityImplement<P extends BaseActivityPresenter>
        extends AppCompatActivity
        implements BaseView {
    private P presenter;
    private View loadingView;


    protected P getPresenter() {
        return presenter;
    }

    abstract P initializePresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        loadingView = findViewById(getLoadingViewId());
        presenter = initializePresenter();
        getPresenter().onCreate();
    }

    protected abstract int getLoadingViewId();

    protected abstract int getLayoutId();

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPresenter().onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onDestroy();

    }


    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().onPause();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


}