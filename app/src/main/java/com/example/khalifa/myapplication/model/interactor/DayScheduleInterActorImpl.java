package com.example.khalifa.myapplication.model.interactor;

import com.example.khalifa.myapplication.model.API.ScheduleApi;
import com.example.khalifa.myapplication.model.Entity.Appointment;
import com.example.khalifa.myapplication.model.Entity.CustomDate;
import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.model.Response.BaseResponse;
import com.example.khalifa.myapplication.model.interactor.interfaces.DayScheduleInterActor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;

public class DayScheduleInterActorImpl extends DayScheduleInterActor {


    private static final int MAX = 6;
    private static final int MIN = 0;

    private HashMap<String, String> createDayScheduleParametersMap(String clinicId, String branchID,
                                                                   String fromDate, String toDate,
                                                                   String doctorID) {
        HashMap<String, String> map = new HashMap<>();
        map.put(ScheduleApi.CLINIC_ID_KEY, clinicId);
        map.put(ScheduleApi.BRANCH_ID_KEY, branchID);
        map.put(ScheduleApi.FROM_DATE_KEY, fromDate);
        map.put(ScheduleApi.TO_DATE_KEY, toDate);
        map.put(ScheduleApi.DOCTOR_ID_KEY, doctorID);
        return map;
    }


    @Override
    public Observable<BaseResponse<ArrayList<DrDaySchedule>>> getSchedule(String clinicId,
                                                                          String branchID,
                                                                          String fromDate,
                                                                          String toDate,
                                                                          String doctorID) {
        return applyScheduler(getRetrofitInstance().create(ScheduleApi.class).getSchedule(
                createDayScheduleParametersMap(clinicId,
                        branchID,
                        fromDate,
                        toDate,
                        doctorID)));
    }

    public Observable<BaseResponse<ArrayList<DrDaySchedule>>> getSchedule(long date) {


        /*return applyScheduler(getRetrofitInstance().create(ScheduleApi.class).getSchedule(
                createDayScheduleParametersMap(clinicId,
                        branchID,
                        fromDate,
                        toDate,
                        doctorID)));*/
        return getTestObservable(date);
    }

    private Observable<BaseResponse<ArrayList<DrDaySchedule>>> getTestObservable(long date) {
        return getTestResponse(date);
    }

    private Observable<BaseResponse<ArrayList<DrDaySchedule>>> getTestResponse(long date) {
        ArrayList<DrDaySchedule> list = new ArrayList<>();
        int size = (int) (Math.random() * (4 + 1));
        if (size < 1)
            size = 2;
        for (int i = 0; i < size; i++)
            list.add(createDaySchedule(date, "dr fn" + i, "dr Ln" + i));

        BaseResponse<ArrayList<DrDaySchedule>> baseResponse = new BaseResponse<>();
        baseResponse.setData(list);
        baseResponse.setResult(true);
        return Observable.just(baseResponse);
    }

    private DrDaySchedule createDaySchedule(long date, String firstName, String lastName) {
        DrDaySchedule drDaySchedule = new DrDaySchedule(
                getAppointments(date, firstName, lastName), new CustomDate(new Date(date)));
        return drDaySchedule;
    }

    private List<Appointment> getAppointments(long date, String drFirstName, String drLastName) {
        ArrayList<Appointment> appointments = new ArrayList<>();
        int size = (int) (Math.random() * (MAX - MIN + 1));

        int startTime = 0;
        for (int i = 0; i < size; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date);
            calendar.set(Calendar.HOUR_OF_DAY, startTime);
            startTime += 2;
            Appointment appointment = new Appointment();
            if (i % 2 == 0)
                appointment.setCompleted(true);
            appointment.setDoctorFirstName(drFirstName);
            appointment.setDoctorLastName(drLastName);
            appointment.setPatientFirstName("fname" + i);
            appointment.setDoctorLastName("lname" + i);
            appointment.setReservationDate(new CustomDate(new Date(calendar.getTimeInMillis())));
            appointment.setPatientReservationDate(new CustomDate(new Date(calendar.getTimeInMillis())));
            appointment.setPatientReservationDate("/Date(" + calendar.getTimeInMillis() + "+0200)/");
            appointment.setReservationDateValue("/Date(" + calendar.getTimeInMillis() + "+0200)/");
            appointments.add(appointment);
        }

        size = (int) (Math.random() * (MAX - MIN + 1));
        startTime = 12;
        for (int i = 0; i < size; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date);
            calendar.set(Calendar.HOUR_OF_DAY, startTime);
            startTime += 2;
            Appointment appointment = new Appointment();
            if (i % 2 == 0)
                appointment.setCompleted(true);
            appointment.setDoctorFirstName(drFirstName);
            appointment.setDoctorLastName(drLastName);
            appointment.setPatientFirstName("fname" + i);
            appointment.setDoctorLastName("lname" + i);
            appointment.setReservationDate(new CustomDate(new Date(calendar.getTimeInMillis())));
            appointment.setPatientReservationDate(new CustomDate(new Date(calendar.getTimeInMillis())));
            appointment.setPatientReservationDate("/Date(" + calendar.getTimeInMillis() + "+0200)/");
            appointment.setReservationDateValue("/Date(" + calendar.getTimeInMillis() + "+0200)/");
            appointments.add(appointment);
        }


        return appointments;
    }
}