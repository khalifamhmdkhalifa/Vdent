package com.example.khalifa.myapplication.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.khalifa.myapplication.R;
import com.example.khalifa.myapplication.view.fragments.MainSchedulesFragment;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeListener;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeObserver;

import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements SelectedTimeObserver, SelectedTimeListener {

    HashSet<SelectedTimeListener> listeners;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_schedule,
                new MainSchedulesFragment()).commit();
    }

    @Override
    public void subscriber(SelectedTimeListener selectedTimeListener) {
        if(listeners==null)
            listeners = new HashSet<>();
        listeners.add(selectedTimeListener);

    }

    @Override
    public void unSubscriber(SelectedTimeListener selectedTimeListener) {
        listeners.remove(selectedTimeListener);
    }

    @Override
    public void onSelectedTimeChanged(int selectedTime) {
        for (SelectedTimeListener listener : listeners)
            listener.onSelectedTimeChanged(selectedTime);
    }
}
