package com.example.khalifa.myapplication.view.interfaces;

public interface SelectedTimeListener {
    void onSelectedTimeChanged(int selectedTime);
}
