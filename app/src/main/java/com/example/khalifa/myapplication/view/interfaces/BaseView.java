package com.example.khalifa.myapplication.view.interfaces;

import android.content.Context;

public interface BaseView {
    Context getContext();

    void showLoading();

    void hideLoading();

    void showErrorMessage(String message);
}