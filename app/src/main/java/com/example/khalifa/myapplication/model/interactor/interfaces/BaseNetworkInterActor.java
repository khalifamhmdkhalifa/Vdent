package com.example.khalifa.myapplication.model.interactor.interfaces;

import com.example.khalifa.myapplication.model.interactor.RetrofitManager;

import retrofit2.Retrofit;

public abstract class BaseNetworkInterActor extends BaseInterActor {
    private Retrofit retrofit;

    public BaseNetworkInterActor() {
        retrofit = getRetroFit();
    }

    private Retrofit getRetroFit() {
        return RetrofitManager.getDefaultRetrofitInstance();
    }

    protected Retrofit getRetrofitInstance() {
        return retrofit;
    }
}