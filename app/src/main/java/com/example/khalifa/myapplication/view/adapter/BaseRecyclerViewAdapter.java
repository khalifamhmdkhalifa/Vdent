package com.example.khalifa.myapplication.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T extends Object, H extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<H> {
    private List<T> items;
    private Context context;

    public BaseRecyclerViewAdapter(Context context, List<T> items) {
        this.items = items;
        this.context = context;
    }

    public BaseRecyclerViewAdapter(Context context) {
        this.items = new ArrayList<>();
        this.context = context;
    }

    @Override
    public H onCreateViewHolder(ViewGroup parent, int viewType) {
        return createHolder(LayoutInflater.from(context)
                .inflate(getItemLayoutId(viewType), null, false),viewType);
    }

    @Override
    public void onBindViewHolder(H holder, final int position) {
        final T item = items.get(position);
        onBindViewHolder(holder, item, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClicked(item, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    abstract int getItemLayoutId(int viewType);

    abstract H createHolder(View rootView, int viewType);

    abstract void onBindViewHolder(H holder, T item, int position);

    abstract void onItemClicked(T item, int position);

    protected Context getContext() {
        return context;
    }

    protected List<T> getItems() {
        return items;
    }
}