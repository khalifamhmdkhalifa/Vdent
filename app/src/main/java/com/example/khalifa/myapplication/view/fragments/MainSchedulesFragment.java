package com.example.khalifa.myapplication.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.khalifa.myapplication.R;
import com.example.khalifa.myapplication.prsenters.MainSchedulePresenterImpl;
import com.example.khalifa.myapplication.prsenters.interfaces.MainSchedulePresenter;
import com.example.khalifa.myapplication.view.adapter.ScheduleViewPagerAdapter;
import com.example.khalifa.myapplication.view.interfaces.BaseFragmentImplement;
import com.example.khalifa.myapplication.view.interfaces.MainScheduleView;

import java.util.Date;
import java.util.List;

import butterknife.BindView;

public class MainSchedulesFragment extends BaseFragmentImplement<MainSchedulePresenter>
        implements MainScheduleView, RadioGroup.OnClickListener {

    @BindView(R.id.viewPager_schedules)
    ViewPager viewPagerSchedules;
    @BindView(R.id.radioGroup_Time)
    RadioGroup radioGroupTime;
    @BindView(R.id.radioButton_AM)
    RadioButton radioButtonAm;
    @BindView(R.id.radioButton_PM)
    RadioButton radioButtonPm;
    @BindView(R.id.textView_Date)
    TextView textViewDate;
    private ScheduleViewPagerAdapter scheduleViewPagerAdapter;

    @Override
    protected MainSchedulePresenter initializePresenter() {
        return new MainSchedulePresenterImpl(this);
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.layout_loading;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_schedule;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        radioGroupTime.setOnClickListener(this);
        radioButtonAm.setOnClickListener(this);
        radioButtonPm.setOnClickListener(this);
    }

    @Override
    public void setupSchedulePager(List<Date> dateList, int selectedTime) {
        viewPagerSchedules.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                getPresenter().onDayViewed(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        scheduleViewPagerAdapter = new ScheduleViewPagerAdapter(getActivity().getSupportFragmentManager(),
                dateList, selectedTime) {
            @Override
            protected void onItemViewed(int position, Date date) {
            }
        };
        viewPagerSchedules.setOffscreenPageLimit(1);
        viewPagerSchedules.setAdapter(scheduleViewPagerAdapter);

    }

    @Override
    public void updatePagerSelectedTime(int selectedTime) {
        if (scheduleViewPagerAdapter != null)
            scheduleViewPagerAdapter.setActivatedTime(selectedTime);
    }

    @Override
    public void notifySchedulesPagerListChanged() {
        if (scheduleViewPagerAdapter != null)
            scheduleViewPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void setCurrentSchedulePagerItem(int position) {
        if (viewPagerSchedules != null)
            viewPagerSchedules.setCurrentItem(position);
    }

    @Override
    public void setDateText(String value) {
        textViewDate.setText(value);
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.radioButton_AM:
                getPresenter().onAmButtonClicked(((RadioButton) view).isChecked());
                break;
            case R.id.radioButton_PM:
                getPresenter().onPmButtonClicked(((RadioButton) view).isChecked());
                break;
        }
    }
}

