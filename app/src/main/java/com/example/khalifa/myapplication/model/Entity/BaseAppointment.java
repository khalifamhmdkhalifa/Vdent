package com.example.khalifa.myapplication.model.Entity;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class BaseAppointment implements Comparable<BaseAppointment> {
    @SerializedName("ReservationDate")
    private String reservationDateValue;//Date(1483221600000+0200)/,
    @SerializedName("ParentReservationDate")
    private String patientReservationDateValue;  ///Date(253402293599999+0200)/,


    public CustomDate getPatientReservationDate() {
        if (patientReservationDate == null)
            patientReservationDate = new CustomDate(patientReservationDateValue);
        return patientReservationDate;
    }

    public CustomDate getReservationDate() {
        if (reservationDate == null)
            reservationDate = new CustomDate(reservationDateValue);
        return reservationDate;
    }

    private CustomDate patientReservationDate;
    private CustomDate reservationDate;


    @Override
    public int compareTo(@NonNull BaseAppointment baseAppointment) {
        return this.getReservationDate().getDate().compareTo(
                baseAppointment.getReservationDate().getDate());
    }

    public void setReservationDate(CustomDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public void setPatientReservationDate(String reservationDateValue) {
        this.patientReservationDateValue = reservationDateValue;
    }

    public void setPatientReservationDate(CustomDate patientReservationDate) {
        this.patientReservationDate = patientReservationDate;
    }

    public void setReservationDateValue(String reservationDateValue) {
        this.reservationDateValue = reservationDateValue;
    }
}
