package com.example.khalifa.myapplication.view.interfaces;

import android.app.Activity;

public interface BaseFragmentView extends BaseView {

    Activity getActivity();

}
