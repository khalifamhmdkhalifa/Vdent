package com.example.khalifa.myapplication.model.Response;

import com.google.gson.annotations.SerializedName;

public class BaseError {
    @SerializedName("ErrorCode")
    private String errorCode;
    @SerializedName("ErrorMSG")
    private String errorMSG;

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMSG() {
        return errorMSG;
    }
}
