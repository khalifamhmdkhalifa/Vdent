package com.example.khalifa.myapplication.prsenters.interfaces;

import com.example.khalifa.myapplication.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.myapplication.view.interfaces.BaseFragmentView;

public abstract class MainSchedulePresenter<T extends BaseFragmentView, I extends BaseInterActor>
        extends BaseFragmentPresenter<T, I> {

    public MainSchedulePresenter(T view) {
        super(view);
    }

    public abstract void onAmButtonClicked(boolean checked);

    public abstract void onPmButtonClicked(boolean checked);

    public abstract void onDayViewed(int position);
}
