package com.example.khalifa.myapplication.model.interactor.interfaces;

import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.model.Response.BaseResponse;

import java.util.ArrayList;

import io.reactivex.Observable;

public abstract class DayScheduleInterActor extends BaseNetworkInterActor {

    public abstract Observable<BaseResponse<ArrayList<DrDaySchedule>>> getSchedule(String clinicId,
                                                                                   String branchID,
                                                                                   String fromDate,
                                                                                   String toDate,
                                                                                   String doctorID);

    public abstract Observable<BaseResponse<ArrayList<DrDaySchedule>>> getSchedule(long date);
}
