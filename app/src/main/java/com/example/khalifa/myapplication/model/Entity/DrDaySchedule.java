package com.example.khalifa.myapplication.model.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

public class DrDaySchedule {
    private static final int START_HOUR = 0;
    private static final int PM_APPOINTMENTS_SIZE = 6;
    private static final int AM_APPOINTMENTS_SIZE = 6;
    private static final int APPOINTMENT_DURATION = 2;
    private static final int AM_HOURS = 12;
    private CustomDate date;

    @SerializedName("Scheduel")
    private List<Appointment> allAppointments;
    private List<Appointment> amAppointments;
    private List<Appointment> pmAppointments;
    @SerializedName("DoctorFirstName")
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    private String doctorLastName;
    @SerializedName("DoctorUserID")
    private String doctorUserId;
    public static final int AM_APPOINTMENTS = 3;
    public static final int PM_APPOINTMENTS = 4;
    private int activeAppointments;


    public void setActiveAppointments(int activeAppointments) {
        this.activeAppointments = activeAppointments;
    }

    public int getActiveAppointmentsType() {
        return activeAppointments;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public DrDaySchedule(List<Appointment> allAppointments, CustomDate date) {
        this.allAppointments = allAppointments;
        this.date = date;
        if (allAppointments != null && allAppointments.size() > 0)
            setupDate(allAppointments.get(0));
    }

    private void setDrData(Appointment appointment) {
        doctorFirstName = appointment.getDoctorFirstName();
        doctorLastName = appointment.getDoctorLastName();
        doctorUserId = appointment.getDoctorUserID();
    }

    public void setupDate(Appointment appointment) {
        setDrData(appointment);
    }

    public List<Appointment> getAllAppointments() {
        return allAppointments;
    }

    public List<Appointment> getAmAppointments() {
        if (amAppointments == null)
            initAmPmAppointments();
        return amAppointments;
    }

    private void initAmPmAppointments() {
        amAppointments = new ArrayList<>();
        pmAppointments = new ArrayList<>();
        for (Appointment appointment : allAppointments)
            if (appointment.getReservationDate().isAmTime())
                amAppointments.add(appointment);
            else
                pmAppointments.add(appointment);
    }

    public List<Appointment> getPmAppointments() {
        if (pmAppointments == null)
            initAmPmAppointments();
        return pmAppointments;
    }


    public static ArrayList<BaseAppointment> getNoGapsActivatedAppointments(DrDaySchedule schedule) {
        if (schedule == null || schedule.getDate() == null)
            return null;
        if (schedule.getActiveAppointmentsType() == DrDaySchedule.AM_APPOINTMENTS) {
            return getNoGapsAMAppointments(schedule.getAmAppointments(), schedule.getDate());
        } else
            return getNoGapsPMAppointments(schedule.getPmAppointments(), schedule.getDate());
    }


    private static ArrayList<BaseAppointment> getNoGapsAMAppointments(
            List<Appointment> amAppointments, CustomDate date) {
        if (amAppointments == null || amAppointments.size() == 0)
            return noGapEmptyAppointmentsList(date, 0);
        Collections.sort(amAppointments);

        if (amAppointments.size() == AM_APPOINTMENTS_SIZE)
            return new ArrayList<BaseAppointment>(amAppointments);
        ArrayList<BaseAppointment> baseAppointments = new ArrayList<>(6);
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        int currentHour = START_HOUR;
        int lastAppointmentStartTime = START_HOUR +
                ((AM_APPOINTMENTS_SIZE - 1) * APPOINTMENT_DURATION);

        int appointmentsIndex = 0;
        while (currentHour <= lastAppointmentStartTime) {
            if (amAppointments != null && amAppointments.size() > appointmentsIndex) {
                Appointment amAppointment = amAppointments.get(appointmentsIndex);
                calendar.setTime(amAppointment.getReservationDate().getDate());
                if (calendar.get(Calendar.HOUR) == currentHour) {
                    baseAppointments.add(amAppointment);
                    appointmentsIndex++;
                } else {
                    baseAppointments.add(createEmptyAppointment(currentHour, date));
                }
            } else
                baseAppointments.add(createEmptyAppointment(currentHour, date));
            currentHour += APPOINTMENT_DURATION;
        }
        return baseAppointments;
    }

    private static BaseAppointment createEmptyAppointment(int hour, CustomDate date) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(date.getDate().getTime());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        EmptyAppointment emptyAppointment = new EmptyAppointment();
        emptyAppointment.setReservationDate(new CustomDate(calendar.getTime()));
        return emptyAppointment;
    }


    private static ArrayList<BaseAppointment> getNoGapsPMAppointments(
            List<Appointment> pmAppointments, CustomDate date) {

        if (pmAppointments == null || pmAppointments.size() == 0)
            return noGapEmptyAppointmentsList(date, AM_HOURS);


        if (pmAppointments.size() == PM_APPOINTMENTS_SIZE)
            return new ArrayList<BaseAppointment>(pmAppointments);

        Collections.sort(pmAppointments);
        ArrayList<BaseAppointment> baseAppointments = new ArrayList<>(6);
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        int currentHour = 12;
        int lastAppointmentStartTime = currentHour +
                ((PM_APPOINTMENTS_SIZE - 1) * APPOINTMENT_DURATION);

        int appointmentsIndex = 0;
        while (currentHour <= lastAppointmentStartTime) {

            if (pmAppointments != null && pmAppointments.size() > appointmentsIndex) {
                Appointment pmAppointment = pmAppointments.get(appointmentsIndex);
                calendar.setTimeInMillis(
                        pmAppointment.getReservationDate().getDate().getTime());
                if (calendar.get(Calendar.HOUR_OF_DAY) == currentHour) {
                    baseAppointments.add(pmAppointment);
                    appointmentsIndex++;
                } else {
                    baseAppointments.add(createEmptyAppointment(currentHour, date));
                }
            } else
                baseAppointments.add(createEmptyAppointment(currentHour, date));

            currentHour += APPOINTMENT_DURATION;
        }
        return baseAppointments;
    }

    private static ArrayList<BaseAppointment> noGapEmptyAppointmentsList(CustomDate date,
                                                                         int hoursOffset) {
        ArrayList<BaseAppointment> baseAppointments = new ArrayList<>(AM_APPOINTMENTS_SIZE);
        int currentHour = START_HOUR + hoursOffset;
        int lastAppointmentStartTime = currentHour +
                ((AM_APPOINTMENTS_SIZE - 1) * APPOINTMENT_DURATION);

        while (currentHour <= lastAppointmentStartTime) {
            baseAppointments.add(createEmptyAppointment(currentHour, date));
            currentHour += APPOINTMENT_DURATION;
        }
        return baseAppointments;
    }

    public CustomDate getDate() {
        if (date == null) {
            if (this.getAllAppointments() != null && getAllAppointments().size() > 0) {
                date = new CustomDate(getAllAppointments().get(0).getReservationDate().getDate());
            }
        }
        return date;
    }


}