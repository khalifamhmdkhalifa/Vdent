package com.example.khalifa.myapplication.model.API;

import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.model.Response.BaseResponse;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ScheduleApi {
    String GET_SCHEDULE_GROUPED_BY_DR = "GetScheduelGroupByDoctor";
    String CLINIC_ID_KEY = "ClinicID";
    String BRANCH_ID_KEY = "BranchID";
    String FROM_DATE_KEY = "FromDate";
    String TO_DATE_KEY = "ToDate";
    String DOCTOR_ID_KEY = "DoctorID";
    /*
     ClinicID :  r5Jy47oxh4dge6ia8LU27A== ,
             BranchID :  Q1TKno3qmm6ecp1of72nSA== ,
             FromDate :  1/1/2017 ,
             ToDate :   ,
             DoctorID :   */

    @Headers({
            "UserToken: V2p3Zn0MoE+HuOjqKtrhiw==",
            "AppID: 123",
            "Content-Type: application/json"
    })
    @POST(GET_SCHEDULE_GROUPED_BY_DR)
    Observable<BaseResponse<ArrayList<DrDaySchedule>>> getSchedule(@Body HashMap<String, String> body);
}
