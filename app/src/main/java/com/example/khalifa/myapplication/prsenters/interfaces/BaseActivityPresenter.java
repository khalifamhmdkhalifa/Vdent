package com.example.khalifa.myapplication.prsenters.interfaces;


import com.example.khalifa.myapplication.view.interfaces.BaseActivityImplement;

public abstract class BaseActivityPresenter<V extends BaseActivityImplement> extends
        BasePresenter {

    public BaseActivityPresenter(V view) {
        super(view);
    }
}