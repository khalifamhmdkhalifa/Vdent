package com.example.khalifa.myapplication.prsenters.interfaces;


import com.example.khalifa.myapplication.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.myapplication.view.interfaces.BaseFragmentView;

public abstract class BaseFragmentPresenter<V extends BaseFragmentView, I extends
        BaseInterActor> extends BasePresenter<V, I> {

    public BaseFragmentPresenter(V view) {
        super(view);
    }

    public abstract void onAttach();

    public abstract void onActivityCreated();

    public abstract void onDestroyView();

    public abstract void onViewCreated();
}
