package com.example.khalifa.myapplication.model.Entity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class CustomDate {
    private Date date;
    private final String DATE_CONTAINER_START = "(";
    private final String DATE_CONTAINER_END = ")";
    private final int HOURS_NUMBER_OF_DIGITS = 2;
    private final int MINUTES_NUMBER_OF_DIGITS = 2;
    private static final int MILLI_SECONDS_IN_Minute = 60000;
    private static final int MILLI_SECONDS_IN_HOUR = 3600000;
    private Calendar calendar;

    //supported format   /Date(1483221600000+0200)/
    public CustomDate(String date) {
        date = date.substring(date.indexOf(DATE_CONTAINER_START) + 1,
                date.indexOf(DATE_CONTAINER_END));
        long dateValueInGMT = getTimeWithDeviceOffset(date);
        long deviceTimeOffset = getDeviceTimeOffset();
        this.date = new Date((dateValueInGMT + deviceTimeOffset));
    }

    public CustomDate(Date time) {
        this.date = time;
    }


    public boolean isAmTime() {

        if (calendar == null) {
            calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(date);
        }

        return calendar.get(Calendar.AM_PM) == Calendar.AM;
    }

    private long getDeviceTimeOffset() {
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int deviceHoursTimeOffset = mTimeZone.getRawOffset();
        return MILLI_SECONDS_IN_HOUR * deviceHoursTimeOffset;
    }

    private long getTimeWithDeviceOffset(String date) {
        String offsetStartChar = "+";
        if (!date.contains(offsetStartChar))
            offsetStartChar = "-";
        int offsetCharPos = date.indexOf(offsetStartChar);
        Long dateValue = Long.parseLong(
                date.substring(0, offsetCharPos));
        int offsetHours = Integer.parseInt(date.substring(offsetCharPos,
                offsetCharPos + HOURS_NUMBER_OF_DIGITS + 1));
        int offsetMinutesStart = offsetCharPos + HOURS_NUMBER_OF_DIGITS + 1;

        int offsetMinutes = Integer.parseInt(
                date.substring(offsetMinutesStart, date.length()));
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int deviceOffset = mTimeZone.getRawOffset();

        return dateValue + ((offsetMinutes * MILLI_SECONDS_IN_Minute) +
                (offsetHours * MILLI_SECONDS_IN_HOUR)) + deviceOffset;
    }

    public Date getDate() {
        return date;
    }
}
