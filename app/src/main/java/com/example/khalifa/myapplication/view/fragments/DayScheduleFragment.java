package com.example.khalifa.myapplication.view.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.example.khalifa.myapplication.R;
import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.prsenters.DaySchedulePresenterImpl;
import com.example.khalifa.myapplication.prsenters.interfaces.DaySchedulePresenter;
import com.example.khalifa.myapplication.view.adapter.BaseRecyclerViewAdapter;
import com.example.khalifa.myapplication.view.adapter.DrsScheduleAdapter;
import com.example.khalifa.myapplication.view.custom.RecyclerViewMargin;
import com.example.khalifa.myapplication.view.interfaces.BaseFragmentImplement;
import com.example.khalifa.myapplication.view.interfaces.DayScheduleView;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeListener;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeObserver;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;

public class DayScheduleFragment extends BaseFragmentImplement<DaySchedulePresenter> implements
        DayScheduleView, SelectedTimeListener {

    private static final String ACTIVATED_TIME_KEY = "selected time";
    @BindView(R.id.recyclerView_daySchedules)
    RecyclerView recyclerViewDaySchedules;
    private static final String DATE_KEY = "date";
    private Date date;
    private BaseRecyclerViewAdapter schedulesAdapter;
    private int activatedTime;

    public static DayScheduleFragment newInstance(Date date, int activatedTime) {
        Bundle args = new Bundle();
        args.putSerializable(DATE_KEY, date);
        args.putSerializable(ACTIVATED_TIME_KEY, activatedTime);
        DayScheduleFragment fragment = new DayScheduleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected DaySchedulePresenter initializePresenter() {
        return new DaySchedulePresenterImpl(this);
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.view_loadingDaySchedule;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_one_day_schedule;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewDaySchedules.addItemDecoration(new RecyclerViewMargin((int) dpToPx(7),
                1));
        recyclerViewDaySchedules.setLayoutManager(layoutManager);
        getPresenter().setup(date, activatedTime);
    }

    private void setData() {
        date = (Date) getArguments().getSerializable(DATE_KEY);
        activatedTime = getArguments().getInt(ACTIVATED_TIME_KEY);
    }


    @Override
    public void notifySchedulesListChanged() {
        if (schedulesAdapter != null && getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    schedulesAdapter.notifyDataSetChanged();
                }
            });
    }

    @Override
    public void setupRecyclerView(ArrayList<DrDaySchedule> drsSchedulesList) {
        schedulesAdapter = new DrsScheduleAdapter(getContext(), drsSchedulesList);
        recyclerViewDaySchedules.setAdapter(schedulesAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((SelectedTimeObserver) getActivity()).subscriber(this);
    }

    @Override
    public void onDestroy() {
        ((SelectedTimeObserver) getActivity()).unSubscriber(this);
        super.onDestroy();

    }

    @Override
    public void onSelectedTimeChanged(int selectedTime) {
        getPresenter().onSelectedTimeChanged(selectedTime);
    }

    private float dpToPx(int dp) {
        Resources resources = getContext().getResources();
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());

    }
}