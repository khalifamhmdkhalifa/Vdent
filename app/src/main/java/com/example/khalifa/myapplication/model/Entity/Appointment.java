package com.example.khalifa.myapplication.model.Entity;

import com.google.gson.annotations.SerializedName;

public class Appointment extends BaseAppointment {
    @SerializedName("Amount")
    private int amount;
    @SerializedName("ApplicationID")
    private String applicationID;
    @SerializedName("CancellationReason")
    private String cancellationReason;
    @SerializedName("Cancelled")
    private boolean cancelled;
    @SerializedName("ClinicID")
    private String clinicID;
    @SerializedName("ClinincBranchID")
    private String clinicBranchID;
    @SerializedName("Completed")
    private boolean completed;
    @SerializedName("ConfirmationDate")
    private String confirmationDate; // /Date(1483221600000+0200)/ ,
    @SerializedName("Confirmed")
    private boolean confirmed;
    @SerializedName("CreationDate")
    private String creationDate;  //Date(1483221600000+0200)/ ,
    @SerializedName("DoctorFirstName")
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    private String doctorLastName;
    @SerializedName("DoctorUserID")
    private String doctorUserID;
    @SerializedName("ID")
    private String id;
    @SerializedName("Modified")
    private String Modified;  //Date(1483221600000+0200)/ ,
    @SerializedName("ModifiedBy")
    private String ModifiedBy;
    @SerializedName("Note")
    private String Note;
    @SerializedName("paied")
    private boolean paid;
    @SerializedName("ParentReservationID")
    private String parentReservationID;
    @SerializedName("PatientFirstName")
    private String patientFirstName;
    @SerializedName("PatientImage")
    private String patientImage;
    @SerializedName("PatientLastName")
    private String patientLastName;
    @SerializedName("PatientUserID")
    private String patientUserID;
    @SerializedName("PaymentMethodID")
    private String paymentMethodId;
    @SerializedName("PaymentMethodName")
    private String paymentMethodName;

    @SerializedName("ReservationNumber")
    private int reservationNumber;

    public int getAmount() {
        return amount;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public String getClinicID() {
        return clinicID;
    }

    public String getClinicBranchID() {
        return clinicBranchID;
    }

    public boolean isCompleted() {
        return completed;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public String getDoctorUserID() {
        return doctorUserID;
    }

    public String getId() {
        return id;
    }

    public String getModified() {
        return Modified;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public String getNote() {
        return Note;
    }


    public boolean isPaid() {
        return paid;
    }

    public String getParentReservationID() {
        return parentReservationID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public String getPatientImage() {
        return patientImage;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public String getPatientUserID() {
        return patientUserID;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public int getReservationNumber() {
        return reservationNumber;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public void setDoctorUserID(String doctorUserID) {
        this.doctorUserID = doctorUserID;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public void setPatientImage(String patientImage) {
        this.patientImage = patientImage;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public void setPatientUserID(String patientUserID) {
        this.patientUserID = patientUserID;
    }

    public void setReservationNumber(int reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
