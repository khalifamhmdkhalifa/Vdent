package com.example.khalifa.myapplication.view.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.example.khalifa.myapplication.R;
import com.example.khalifa.myapplication.model.Entity.BaseAppointment;
import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;
import com.example.khalifa.myapplication.view.custom.RecyclerViewMargin;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrsScheduleAdapter extends BaseRecyclerViewAdapter<DrDaySchedule,
        DrsScheduleAdapter.Holder> {


    public DrsScheduleAdapter(Context context, List<DrDaySchedule> items) {
        super(context, items);
    }

    @Override
    int getItemLayoutId(int viewType) {
        return R.layout.list_item_dr_schedule;
    }

    @Override
    Holder createHolder(View rootView, int viewType) {
        return new Holder(rootView);
    }

    @Override
    void onBindViewHolder(Holder holder, DrDaySchedule item, int position) {
        holder.textViewDrName.setText(item.getDoctorFirstName() + " " + item.getDoctorLastName());
        holder.setupRecyclerView(DrDaySchedule.getNoGapsActivatedAppointments(item));
    }

    @Override
    void onItemClicked(DrDaySchedule item, int position) {

    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_DrName)
        TextView textViewDrName;
        @BindView(R.id.recyclerView_appointments)
        RecyclerView recyclerViewAppointments;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            recyclerViewAppointments.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerViewAppointments.addItemDecoration(new RecyclerViewMargin(
                    (int) dpToPx(10), 1));

        }

        void setupRecyclerView(List<BaseAppointment> appointments) {
            AppointmentsAdapter appointmentsAdapter = new AppointmentsAdapter(getContext(),
                    appointments);
            recyclerViewAppointments.setAdapter(appointmentsAdapter);
        }
    }


    private float dpToPx(int dp) {
        Resources resources = getContext().getResources();
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());

    }
}
