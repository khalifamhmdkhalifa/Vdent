package com.example.khalifa.myapplication.view.interfaces;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.khalifa.myapplication.prsenters.interfaces.BaseFragmentPresenter;

import butterknife.ButterKnife;

public abstract class BaseFragmentImplement<P extends BaseFragmentPresenter> extends
        Fragment implements BaseFragmentView {
    private P presenter;
    View loadingView;


    protected P getPresenter() {
        return presenter;
    }

    protected abstract P initializePresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().onCreate();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        loadingView = view.findViewById(getLoadingViewId());
        return view;
    }


    protected abstract int getLoadingViewId();

    protected abstract int getLayoutId();

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPresenter().onDestroy();

    }


    @Override
    public void onPause() {
        super.onPause();
        getPresenter().onPause();
    }

    @Override
    public void showLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPresenter().onViewCreated();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = initializePresenter();
        getPresenter().onAttach();
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}