package com.example.khalifa.myapplication.view.interfaces;

import com.example.khalifa.myapplication.model.Entity.DrDaySchedule;

import java.util.ArrayList;

public interface DayScheduleView extends BaseFragmentView {
    void notifySchedulesListChanged();

    void setupRecyclerView(ArrayList<DrDaySchedule> drsSchedulesList);
}
