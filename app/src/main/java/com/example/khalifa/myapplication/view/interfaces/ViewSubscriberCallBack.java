package com.example.khalifa.myapplication.view.interfaces;

public interface ViewSubscriberCallBack<T extends Object> {

    void onSuccess(T result);

    void onError(Throwable throwable);
}