package com.example.khalifa.myapplication.prsenters.interfaces;

import com.example.khalifa.myapplication.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.myapplication.view.interfaces.BaseFragmentView;
import com.example.khalifa.myapplication.view.interfaces.SelectedTimeListener;

import java.util.Date;

public abstract class DaySchedulePresenter<T extends BaseFragmentView, I extends BaseInterActor>
        extends BaseFragmentPresenter<T, I> implements SelectedTimeListener {

    public DaySchedulePresenter(T view) {
        super(view);
    }

    public abstract void setup(Date date, int activatedTime);

}
